#include "Pawn.h"

bool Pawn::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  int col = (start.first - end.first);
  int row = (start.second - end.second);
  if (to_ascii() == 'p' && col == 0) {
    if (start.second == '7') {
      if (row == 2 || row == 1) {
        return true;
      }
    }
    else if (row == 1) {
      return true;
    }
  }

  else if (to_ascii() == 'P' && col == 0) {
    if (start.second == '2') {
      if (row == -1 || row == -2) {
        return true;
      }
    }
    else if (row == -1) {
      return true;
    }
  }
  return false;
}


bool Pawn::legal_capture_shape(std::pair<char, char> start,std::pair<char, char> end) const {
  int col = start.first - end.first;
  int row = start.second - end.second;
   
  if (to_ascii() == 'P') {
    if (row == -1 && (col == 1 || col == -1)) {
      return true;
    }
  }
  
  else if (to_ascii() == 'p') {
    if (row == 1 && (col == 1 || col == -1)) {
      return true;
    }
  }
  return false;
}


