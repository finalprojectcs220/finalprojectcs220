\1;4205;0cCONSERVATIVE_FLAGS = -std=c++11 -Wall -Wextra -pedantic
DEBUGGING_FLAGS = -g -O0
CFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)

chess: main.o CreatePiece.o Chess.o Board.o King.o Queen.o Knight.o Bishop.o Rook.o Pawn.o
	g++ -o chess main.o CreatePiece.o Chess.o Board.o King.o Queen.o Knight.o Bishop.o Rook.o Pawn.o

main.o:main.cpp Chess.h
	g++ -c main.cpp $(CFLAGS)
CreatePiece.o: CreatePiece.cpp CreatePiece.h Piece.h
	g++ -c CreatePiece.cpp $(CFLAGS)
Board.o: Board.cpp Board.h Mystery.h Piece.h King.h Queen.h Knight.h Bishop.h Rook.h Pawn.h
	g++ -c Board.cpp $(CFLAGS)
Chess.o: Chess.cpp Chess.h Board.h Piece.h
	g++ -c Chess.cpp $(CFLAGS)
King.o: King.cpp King.h Piece.h
	g++ -c King.cpp $(CFLAGS)
Queen.o: Queen.cpp Queen.h Piece.h
	g++ -c Queen.cpp $(CFLAGS)
Knight.o: Knight.cpp Knight.h Piece.h
	g++ -c Knight.cpp $(CFLAGS)
Bishop.o: Bishop.cpp Bishop.h Piece.h
	g++ -c Bishop.cpp $(CFLAGS)
Rook.o: Rook.cpp Rook.h Piece.h
	g++ -c Rook.cpp $(CFLAGS)
Pawn.o: Pawn.cpp Pawn.h Piece.h
	g++ -c Pawn.cpp $(CFLAGS)
.PHONY: clean
clean:
	rm -f *.o chess *~
