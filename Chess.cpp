#include "Chess.h"
#include <iterator>
#include <vector>
/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Chess::Chess() : is_white_turn(true) {
	// Add the pawns
	for (int i = 0; i < 8; i++) {
		board.add_piece(std::pair<char, char>('A' + i, '1' + 1), 'P');
		board.add_piece(std::pair<char, char>('A' + i, '1' + 6), 'p');
	}

	// Add the rooks
	board.add_piece(std::pair<char, char>( 'A'+0 , '1'+0 ) , 'R' );
	board.add_piece(std::pair<char, char>( 'A'+7 , '1'+0 ) , 'R' );
	board.add_piece(std::pair<char, char>( 'A'+0 , '1'+7 ) , 'r' );
	board.add_piece(std::pair<char, char>( 'A'+7 , '1'+7 ) , 'r' );

	// Add the knights
	board.add_piece(std::pair<char, char>( 'A'+1 , '1'+0 ) , 'N' );
	board.add_piece(std::pair<char, char>( 'A'+6 , '1'+0 ) , 'N' );
	board.add_piece(std::pair<char, char>( 'A'+1 , '1'+7 ) , 'n' );
	board.add_piece(std::pair<char, char>( 'A'+6 , '1'+7 ) , 'n' );

	// Add the bishops
	board.add_piece(std::pair<char, char>( 'A'+2 , '1'+0 ) , 'B' );
	board.add_piece(std::pair<char, char>( 'A'+5 , '1'+0 ) , 'B' );
	board.add_piece(std::pair<char, char>( 'A'+2 , '1'+7 ) , 'b' );
	board.add_piece(std::pair<char, char>( 'A'+5 , '1'+7 ) , 'b' );

	// Add the kings and queens
	board.add_piece(std::pair<char, char>( 'A'+3 , '1'+0 ) , 'Q' );
	board.add_piece(std::pair<char, char>( 'A'+4 , '1'+0 ) , 'K' );
	board.add_piece(std::pair<char, char>( 'A'+3 , '1'+7 ) , 'q' );
	board.add_piece(std::pair<char, char>( 'A'+4 , '1'+7 ) , 'k' );
}

bool Chess::make_move(std::pair<char, char> start, std::pair<char, char> end) {
  if (board(start) == NULL) {
    return false;
  }
  bool is_white = board(start)->is_white();
  // Check if path is valid (not blocked)
  if (board.check_path(start, end) && is_white == is_white_turn) {
    // Open end position -> legal move shape?
    if (board(end) == NULL && board(start)->legal_move_shape(start, end)) {
      if (!promote_pawn(start, end)) {  // Check for promotion and add start piece if false
        board.add_piece(end, board(start)->to_ascii());
      }
      board.remove_piece(start);
      if (in_check(is_white)) {  // Revert board state if move threatens your own king
	board.add_piece(start, board(end)->to_ascii());
	board.remove_piece(end);
	return false;
      }
      is_white_turn = !is_white_turn;
      return true;
    }
    // Occupied end position -> legal capture shape?
    if (board(end) != NULL && board(end)->is_white() == is_white) {  // Cannot capture own pieces
      return false;
    }
    if (board(end) != NULL && board(start)->legal_capture_shape(start, end)) {
      char temp = board(end)->to_ascii();  // Store end piece in case we need to revert
      board.remove_piece(end);
      if (!promote_pawn(start, end)) {  // Check for promotion and add start piece if false 
        board.add_piece(end, board(start)->to_ascii());
      }
      board.remove_piece(start);
      if (in_check(is_white)) { // Revert board state if capture threatens your own king
	board.add_piece(start, board(end)->to_ascii());
	board.remove_piece(end);
	board.add_piece(end, temp);
	return false;
      }
      is_white_turn = !is_white_turn;
      return true;
    }
    return false;
  }	
  return false;
}

bool Chess::promote_pawn(std::pair<char, char> start, std::pair<char, char> end) {
  // Check to promote a white pawn
  if (start.second == '7' && end.second == '8') {
    if (board(start)->to_ascii() == 'P') {
      board.add_piece(end, 'Q');
      return true;
    }
  }
  // Check to promote a black pawn
  else if (start.second == '2' && end.second == '1') {
    if (board(start)->to_ascii() == 'p') {
      board.add_piece(end, 'q');
      return true;
    }
  }
  return false;
}

bool Chess::in_check(bool white) const {
  char king_char;
  if (white) {
    king_char = 'K';
  } else {
    king_char = 'k';
  }
  
  std::pair<char, char> king_loc;  // Find the location of the king
  for (char col = 'A'; col < 'I'; col++) {
    for (char row = '1'; row < '9'; row++) {
      king_loc = std::make_pair(col, row);
      if (board(king_loc) && board(king_loc)->to_ascii() == king_char) {
	col = 'I';
	break;
      }
    }
  }

  // Check every opponent piece to see if they can capture your king
  std::pair<char, char> opp_loc;
  for (char col = 'A'; col < 'I'; col++) {
    for (char row = '1'; row < '9'; row++) {
      opp_loc = std::make_pair(col, row);
      // If location is an enemy piece, has open path, and
      // enemy piece has legal capture shape, then king is checked.
      if (board(opp_loc) && board(opp_loc)->is_white() != white &&
	  board.check_path(opp_loc, king_loc) &&
	  board(opp_loc)->legal_capture_shape(opp_loc, king_loc)) {
	return true;
      }
    }
  }
  return false;
}

bool Chess::in_mate(bool white) const {
  if (!in_check(white)==false){
    return false;
  }
  Board copy(board);
  // check if any move can move the king out of check  
  for (const std::pair<char, char>& start: get_board()) {
    for (char col = 'A'; col < 'I'; col++) {
      for (char row = '1'; row < '9'; row++) {	
	std::pair<char, char> end (col, row);
	if (board(start)->is_white() && (board(end) == NULL || !board(end)->is_white())) {
	  if (board(start)->legal_move_shape(start, end) && board(end) == NULL
	      && board.check_path(start, end)) {
	    copy.add_piece(end, copy(start)->to_ascii());
	    copy.remove_piece(start);
	    bool checked = in_check(white);
	    copy.add_piece(start, copy(end)->to_ascii());
	    copy.remove_piece(end);
	    if (!checked){
	      return false;
	    }
	  }
	  else if (board(start)->legal_capture_shape(start, end) && board(end) != NULL && board.check_path(start, end)) {
	   char temp = board(end)->to_ascii();   
	    copy.remove_piece(end);
	    copy.add_piece(end, copy(start)->to_ascii());
	    copy.remove_piece(start);
	    bool checked = in_check(white);
	    copy.add_piece(start, copy(end)->to_ascii()); 
	    copy.remove_piece(end);
	    copy.add_piece(end, temp);
	    if (!checked){
	      return false;
	    }
	  }
	}
      }
    }
  }
  return true;
}

bool Chess::in_stalemate(bool white) const {
  if (in_check(white)==true){
    return false;
  }
  Board copy(board);
  for (const std::pair<char,char>& elem:get_board()){
    for (char col = 'A'; col < 'I'; col++) {
      for (char row = '1'; row < '9'; row++) {
	std::pair<char, char> pos (col, row);
	if (board(elem)->is_white() && (board(pos) == NULL || !board(pos)->is_white())) {
	    if (board(elem)-> legal_move_shape(elem, pos) && board(pos) == NULL && board.check_path(elem, pos)) {
	      copy.add_piece(pos, copy(elem)->to_ascii());
	      copy.remove_piece(elem);
	      bool checked = in_check(white);
	      copy.add_piece(elem, copy(pos)->to_ascii());
	      copy.remove_piece(pos);
	      if (!checked){
		return false;
	      }
	    }
	    else if (board(elem)->legal_capture_shape(elem, pos) && board(pos) != NULL && board.check_path(elem, pos)) {
	      char temp = board(pos)->to_ascii();
	      copy.remove_piece(pos);
	      copy.add_piece(pos, copy(elem)->to_ascii());
	      copy.remove_piece(elem);
	      bool checked = in_check(white);
	      copy.add_piece(elem, copy(pos)->to_ascii());
	      copy.remove_piece(pos);
	      copy.add_piece(pos, temp);
	      if (!checked){
		return false;
	      }
	    }
	}
      }
    }
  }
  return true;
}



/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator<< (std::ostream& os, const Chess& chess) {
	// Write the board out and then either the character 'w' or the character 'b',
	// depending on whose turn it is
	return os << chess.get_board() << (chess.turn_white() ? 'w' : 'b');
}


std::istream& operator>> (std::istream& is, Chess& chess) {
  chess.clear_board();
  // create new char to store into the board
  char c;
  for (int i = 8; i > 0; i--) {
    for (int j = 0; j < 8; j++) {
      is >> c;
      chess.add_piece_board(std::pair<char, char>( 'A'+j , '0'+i ), c);
    }
  }
  // check if it is white's trun or black's turn
  is >> c;
  switch (c) {
  case 'w':
    chess.set_turn(true);
    break;
  case 'b':
    chess.set_turn(false);
    break;
  default:
    chess.set_turn(true);
  }
  if (!chess.check_king()) {chess.clear_board();}// clear the board if the file is invalid
  return is;
}
