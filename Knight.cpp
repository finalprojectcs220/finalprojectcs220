#include "Knight.h"
#include <cmath>

using std::abs;

bool Knight::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  if (((start.first - end.first == 2) && (start.second - end.second == 1)) ||
      ((start.first - end.first == 2) && (end.second - start.second == 1)) ||
      ((start.first - end.first == 1) && (start.second - end.second == 2)) ||
      ((start.first - end.first == 1) && (end.second - start.second == 2)) ||
      ((end.first - start.first == 2) && (end.second - start.second == 1)) ||
      ((end.first - start.first == 2) && (start.second - end.second == 1)) ||
      ((end.first - start.first == 1) && (end.second - start.second == 2)) ||
      ((end.first - start.first == 1) && (start.second - end.second == 2))) {
    return true;
  }
  return false;
}
