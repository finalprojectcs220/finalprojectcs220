#include "King.h"

bool King::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  int x_distance = end.first - start.first;
  int y_distance = end.second - start.second;
  if (x_distance >= -1 && x_distance <= 1) {
    if (y_distance >= -1 && y_distance <= 1) {
      return true;
    }
  }
  return false;
}
