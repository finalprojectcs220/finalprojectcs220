#include "Terminal.h"
#include <iostream>
#include <utility>
#include <cmath>
#include <map>
#include "Board.h"
#include "CreatePiece.h"


/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Board::Board(){}

const Piece* Board::operator()(std::pair<char, char> position) const {
  if (occ.find(position) != occ.cend()){
    return occ.find(position)->second;
  }
  return NULL;
}

Board::~Board(){
  clear();
}

Board::Board(const Board& old){
  for (std::map<std::pair<char, char>, Piece*>::const_iterator it = old.occ.cbegin();
	it != old.occ.cend();
	it++) {
    occ[it->first]=create_piece(it->second->to_ascii());
  }
}

void Board::clear() {
  for (std::map<std::pair<char, char>, Piece*>::iterator it = occ.begin();
        it != occ.end();
        it++) {
    delete it->second;
  }
}

bool Board::check_path(std::pair<char,char> start, std::pair<char,char> end) const {
  if (!this->valid_pos(start) || !this->valid_pos(end)) {
    return false;
  }
  if (occ.find(start) == occ.cend()) {
    return false;
  }
  const Piece* piece = occ.at(start);
  if (!piece->legal_move_shape(start, end) &&
      !piece->legal_capture_shape(start, end)) {
    return false;
  }
  
  int x_distance = start.first - end.first;
  int y_distance = start.second - end.second;
  int begin;
  int fin;
  
  if (y_distance == 0) {  // Moving horizontally
    if (x_distance < 0) {  // To the right
      begin = start.first + 1;
      fin = end.first;
    }
    else { // To the left
      begin = end.first + 1;
      fin = start.first;
    }
    for (int c = begin; c < fin; c++) {
      std::pair<char, char> id ((char) c, start.second);
      if (!(occ.find(id) == occ.cend())) {
	return false;
      }
    }
  }
  else if (x_distance == 0) { // Moving vertically
    if (y_distance < 0) { // Upwards
      begin = start.second + 1;
      fin = end.second;
    }
    else {  // Downwards
      begin = end.second + 1;
      fin = start.second;
    }
    for (int c = begin; c < fin; c++) {
      std::pair<char,char> id (start.first, (char) c);
      if (!(occ.find(id) == occ.cend())) {
        return false;
      }
    }
  }
  else if (abs(x_distance) == abs(y_distance)) {
    if (x_distance < 0) { // To the right
      if (y_distance < 0) { // Upwards
        for(int i = 0; i < abs(x_distance); i++) {
	  std::pair<char, char> id ((char) (start.first + 1 + i), (char) (start.second + 1 + i));
	  if (!(occ.find(id) == occ.end())) {
	    return false;
	  }
        }
      } else {  // Downwards
	for (int i = 0; i < abs(x_distance); i++) {
	  std::pair<char, char> id ((char) (start.first + 1 + i), (char) (start.second - 1 - i));
	  if (!(occ.find(id) == occ.cend())) {
	    return false;
	  }
	}
      }
    }
    else { // To the left
      if (y_distance < 0) { // Upwards
        for(int i = 0; i < abs(x_distance); i++) {
          std::pair<char, char> id ((char) (start.first - 1 - i), (char) (start.second + 1 + i));
          if(!(occ.find(id) == occ.cend())) {
            return false;
          }
	}
      } else { // Downwards
	for (int i = 0; i < abs(x_distance); i++) {
	  std::pair<char, char> id ((char) (start.first - 1 - i), (char) (start.second - 1 - i));
	  if (!(occ.find(id) == occ.cend())) {
	    return false;
	  }
	}
      }
    }
  }
  return true;
}
    
bool Board::add_piece(std::pair<char, char> position, char piece_designator) {
  if (!this->valid_pos(position)) {
    return false;
  }
  // Switched from bracket operator to find to stop annoying behavior
  if (!(occ.find(position) == occ.end())) {
    return false;
  }
  occ[position] = create_piece(piece_designator);
  return true;
}

bool Board::remove_piece(std::pair<char, char> position) {
  if (!this->valid_pos(position)) {
    return false;
  }
  delete occ[position];
  occ.erase(position);
  return true;
}

bool Board::valid_pos(std::pair<char,char> position) const {
  if ((position.first < 'A') || (position.first > 'H') || (position.second < '1') || (position.second > '8')) {
    return false;
  }
  return true;
}

bool Board::has_valid_kings() const {
  int kings = 0;
  for(std::map<std::pair<char,char>, Piece*>::const_iterator it = occ.cbegin();
      it != occ.cend();
      ++it) {
    if(it->second->to_ascii() == 'k' || it->second->to_ascii() == 'K') {
      kings++;
    }
    if (kings > 2) {
      return false;
    }
    if (kings == 2) {
      return true;
    }
  }
  return false;
}

void Board::display() const {
  bool color = true;
  for(char r = '8'; r >= '1'; r--) {
   color=!color;
   for(char c = 'A'; c <= 'H'; c++) {
      const Piece* piece = nullptr;
      if (color){
        Terminal::color_bg(Terminal::YELLOW);
      }
      if (!color){
        Terminal::color_bg(Terminal::DEFAULT_COLOR);
      }

      if (occ.count(std::pair<char,char>(c,r))) {
	piece = occ.at(std::pair<char,char>(c,r));
      }
      if (piece) {
	Terminal::color_fg(true,Terminal::WHITE);
	char c = piece->to_ascii();
	if (c == 'K'){
	  std::cout << "\u2654 ";
	}
	else if (c == 'k'){
	  std::cout << "\u265a ";
	}
	else if (c == 'Q'){
	  std::cout << "\u2655 ";
	}
	else if (c == 'q'){
	  std::cout << "\u265b ";
	}
	else if (c == 'R'){
	  std::cout << "\u2656 ";
	}
	else if	(c == 'r'){
          std::cout << "\u265c ";
	}
	else if	(c == 'B'){
          std::cout << "\u2657 ";
	}
	else if	(c == 'b'){
          std::cout << "\u265d ";
	}
	else if	(c == 'N'){
          std::cout << "\u2658 ";
	}
	else if	(c == 'n'){
          std::cout << "\u265e ";
	}
	else if	(c == 'P'){
          std::cout << "\u2659 ";
	}
	else if	(c == 'p'){
          std::cout << "\u265f ";
	}
      }
      else {
	std::cout<< "  ";
      }
      /*      if (color){
	Terminal::color_bg(Terminal::YELLOW);
      }
      if (!color){
	Terminal::color_bg(Terminal::BLUE);
	}*/
      Terminal::set_default();
      color =!color;
   }
   std::cout<<std::endl;
  }
}

//    Terminal::color_bg(Terminal::YELLOW);
//    c++;}
//      Terminal::color_bg(Terminal::BLUE);
      //Terminal::color_bg(Terminal::GREEN);
      
     /*for(char i = '8'; i >= '1'; i--) {
    for(char j = 'A'; j <= 'H'; j++) {
      std::pair<char, char> id(j,i);
      if(occ.find(id) == occ.cend()) {
	std::cout << "-";
      }
      else {
	const Piece* piece = occ.at(id);
	std::cout << (piece->to_ascii());
      }
    }
    std::cout << std::endl;
  }
  }*/

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator<<(std::ostream& os, const Board& board) {
  for(char r = '8'; r >= '1'; r--) {
    for(char c = 'A'; c <= 'H'; c++) {
      const Piece* piece = board(std::pair<char, char>(c, r));
      if (piece) {
	os << piece->to_ascii();
      } else {
	os << '-';
      }
    }
    os << std::endl;
  }
  return os;
}
