#include "Bishop.h"

bool Bishop::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  // check the range of the checker board
  if ((start.first - end.first) == -(start.second - end.second) ||
      (start.first - end.first) == (start.second - end.second)) {
    return true;
  }
  else {
    return false;
  }
}
      
