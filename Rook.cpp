#include "Rook.h"

bool Rook::legal_move_shape(std::pair<char,char> start, std::pair<char, char> end) const{
  if((start.first - end.first) == 0 || (start.second - end.second) == 0) {
    return true;
  }
  return false;
}
