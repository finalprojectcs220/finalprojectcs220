#ifndef CHESS_H
#define CHESS_H

#include <cstdlib>
#include <iostream>
#include "Piece.h"
#include "Board.h"

class Chess {

public:
  // This default constructor initializes a board with the standard
  // piece positions, and sets the state to white's turn
  Chess();
  ~Chess() {delete &board;}
  // Returns a constant reference to the board 
  /////////////////////////////////////
  // DO NOT MODIFY THIS FUNCTION!!!! //
  /////////////////////////////////////
  const Board& get_board() const { return board; }
  
  // Returns true if it is white's turn
  /////////////////////////////////////
  // DO NOT MODIFY THIS FUNCTION!!!! //
  /////////////////////////////////////
  bool turn_white() const { return is_white_turn; }
  
  // Attemps to make a move. If successful, the move is made and
  // the turn is switched white <-> black
  bool make_move(std::pair<char, char> start, std::pair<char, char> end);

  // Check to see if a pawn needs to be promoted
  bool promote_pawn(std::pair<char, char> start, std::pair<char, char> end);
  
  // Returns true if the designated player is in check
  bool in_check(bool white) const;
  
  // Returns true if the designated player is in mate
  bool in_mate(bool white) const;
  
  // Returns true if the designated player is in mate
  bool in_stalemate(bool white) const;
  
  bool add_piece_board(std::pair<char, char> pos, char ch) { return board.add_piece(pos, ch); }

  // Set the next turn black or white
  void set_turn(bool is_white) { is_white_turn = is_white; }

  // clear the content of the board
  void clear_board() { board.clear(); }

  // check if the board has valid kings
  bool check_king() { return board.has_valid_kings(); }

private:
  // The board
  Board board;
  
  // Is it white's turn?
  bool is_white_turn;
  
};

// Writes the board out to a stream
std::ostream& operator<< (std::ostream& os, const Chess& chess);

// Reads the board in from a stream
std::istream& operator>> (std::istream& is, Chess& chess);


#endif // CHESS_H
